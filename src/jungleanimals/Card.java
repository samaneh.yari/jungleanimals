package jungleanimals;

import java.util.Random;

enum CardType {Lion, Bear, Tiger, Vulture, Fox, Elephant, Wolf, Hog, Hippopotamus, Cow, Rabbit, Turtle}

public class Card {
    private CardType type;
    private int attack1, attack2;
    private int energy;
    private int lostEnergy;
    private int health;
    private int lostHealth;

    public Card(CardType type) {
        this.type = type;
        switch (type) {
            case Lion:
                this.attack1 = 150;
                this.attack2 = 500;
                this.energy = 1000;
                this.health = 900;
                break;
            case Bear:
                this.attack1 = 130;
                this.attack2 = 600;
                this.energy = 900;
                this.health = 850;
                break;
            case Tiger:
                this.attack1 = 120;
                this.attack2 = 650;
                this.energy = 850;
                this.health = 850;
                break;
            case Vulture:
                this.attack1 = 100;
                this.attack2 = 100;
                this.energy = 600;
                this.health = 350;
                break;
            case Fox:
                this.attack1 = 90;
                this.attack2 = 90;
                this.energy = 600;
                this.health = 400;
                break;
            case Elephant:
                this.attack1 = 70;
                this.attack2 = 50;
                this.energy = 500;
                this.health = 1200;
                break;
            case Wolf:
                this.attack1 = 700;
                this.attack2 = 700;
                this.energy = 700;
                this.health = 450;
                break;
            case Hog:
                this.attack1 = 80;
                this.attack2 = 80;
                this.energy = 500;
                this.health = 1100;
                break;
            case Hippopotamus:
                this.attack1 = 110;
                this.attack2 = 110;
                this.energy = 360;
                this.health = 1000;
                break;
            case Cow:
                this.attack1 = 90;
                this.attack2 = 100;
                this.energy = 400;
                this.health = 750;
                break;
            case Rabbit:
                this.attack1 = 80;
                this.attack2 = 80;
                this.energy = 350;
                this.health = 200;
                break;
            case Turtle:
                this.attack1 = 200;
                this.attack2 = 200;
                this.energy = 230;
                this.health = 350;
                break;
        }
        lostEnergy = 0;
        lostHealth = 0;
    }

    public boolean hit(int dmg) {
        if (lostHealth + dmg >= health)
            return true;
        lostHealth += dmg;
        return false;
    }

    public void heal() {
        lostEnergy = 0;
    }

    public boolean checkEnergy(int lost) {
        if (energy - lostEnergy - lost < 0) return false;
        return true;
    }

    public void energyReduction(int lost) {
        lostEnergy += lost;
    }

    public int getAttackPoint() {
        Random random = new Random();
        if (random.nextBoolean())
            return attack1;
        return attack2;
    }

    public String getStringType() {
        return type.toString();
    }

    public int getAttack1() {
        return attack1;
    }

    public int getAttack2() {
        return attack2;
    }

    public int getEnergy() {
        return energy - lostEnergy;
    }

    public int getHealth() {
        return health - lostHealth;
    }

}
