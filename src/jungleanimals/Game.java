package jungleanimals;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Game {
    Player p1, p2;

    public Game() {
        p1 = new Player();
        p2 = new Player();
    }

    public void generateCards(Player p) {
        ArrayList<Card> possibles = new ArrayList<>();
        for (int i = 0; i < 12; i++)
            for (int j = 0; j < 5; j++)
                possibles.add(new Card(CardType.values()[i]));

        Random random = new Random();
        for(int i = 0; i < 30; i++) {
            int rand = random.nextInt(possibles.size());
            p.AddCard(possibles.get(rand));
            possibles.remove(rand);
        }
    }

    public void printBoard(Player p) {
        System.out.println(" ID           Name   A1    A2 Energy Health");
        System.out.println("-------------------------------------------");
        for (int i = 0; i < p.getRemainingCards(); i++) {
            System.out.printf("%2d) %13s  %4d   ", i, p.getCard(i).getStringType(), p.getCard(i).getAttack1());
            if (p.getCard(i).getAttack1() == p.getCard(i).getAttack2())
                System.out.print("  -   ");
            else
                System.out.printf("%3d   ", p.getCard(i).getAttack2());
            System.out.printf("%4d   %4d\n", p.getCard(i).getEnergy(), p.getCard(i).getHealth());
        }
    }


    public void selectCards(Player p) {
        System.out.print("\nSelect 10 cards by ID: ");
        Scanner sc = new Scanner(System.in);
        ArrayList<Card> selected = new ArrayList<>();
        Random random = new Random();
        int j = 0;
        while (j < 10) {
            if(p.getType() == PlayerType.Computer) {
                int rand = random.nextInt(30);
                if (selected.contains(p.getCard(rand))) {
                    j--;
                    continue;
                }
                selected.add(p.getCard(rand));
                System.out.print(rand + " ");
            }
            else {
                int input = sc.nextInt();
                boolean badinput = false;
                while (input < 0 || input > 29) {
                    System.out.println(input + " is not valid");
                    input = sc.nextInt();
                    badinput = true;
                }
                if (badinput)
                    System.out.print("Enter valid IDs: ");
                selected.add(p.getCard(input));
            }
            j++;
        }

        if(p.getType() == PlayerType.Computer)
            System.out.println();

        int i = 29;
        while (i >= 0) {
            boolean flag = false;
            for (int k = 0; k < 10; k++)
                if (p.getCard(i) == selected.get(k))
                    flag = true;

            if (!flag)
                p.removeCard(p.getCard(i));
            i--;
        }
    }

    public void setPlayers() {
        System.out.print("1 Player(With Computer)=1, 2 Player=2: ");
        Scanner sc = new Scanner(System.in);
        p1.setType(PlayerType.User);
        if (sc.nextInt() == 1)
            p2.setType(PlayerType.Computer);
        else
            p2.setType(PlayerType.User);
    }

    public void start() {

        setPlayers();

        System.out.println("\nPlayer (1)");
        generateCards(p1);
        printBoard(p1);
        selectCards(p1);
        printBoard(p1);

        System.out.println("\nPlayer (2)");
        generateCards(p2);
        printBoard(p2);
        selectCards(p2);
        printBoard(p2);

        boolean p1Turn = true;
        Scanner sc = new Scanner(System.in);
        System.out.println("\n********************");
        System.out.println("****** START *******");
        System.out.println("********************");
        while (true) {

            if (p1Turn) {
                boolean attackFlag = true;
                if (p1.getHealOp() > 0) {
                    System.out.print("Player 1 --> Heal(1) or Attack(2)?: ");
                    if(sc.nextInt() == 1) {
                        System.out.print("Enter your card ID to Heal: ");
                        int selected = sc.nextInt();
                        while(selected < 0 || selected > p1.getRemainingCards()) {
                            System.out.println(selected + " is not valid");
                            selected = sc.nextInt();
                        }
                        p1.heal(selected);
                        printBoard(p1);
                        attackFlag = false;
                    }
                }
                if (attackFlag) {
                    System.out.println();
                    System.out.println("\nPlayer (1)");
                    printBoard(p1);
                    System.out.println("\nPlayer (2)");
                    printBoard(p2);
                    System.out.print("\nPlayer 1 --> How many cards do you want to pick? ");
                    int count = sc.nextInt();
                    System.out.print("Enter your cards for attack: ");
                    ArrayList<Card> attackers = new ArrayList<>();
                    while(count > 0) {
                        int selected = sc.nextInt();
                        while(selected < 0 || selected > p1.getRemainingCards()) {
                            System.out.println(selected + " is not valid");
                            selected = sc.nextInt();
                        }
                        attackers.add(p1.getCard(selected));
                        count--;
                    }
                    System.out.print("Enter opponent card to attack: ");
                    int opponentID = sc.nextInt();
                    if (!p1.attack(attackers, p2, opponentID)) {
                        System.out.println("***************************************************");
                        System.out.println("**** Out of Energy. Please select another card ****");
                        System.out.println("***************************************************");
                        continue;
                    }
                }
                p1Turn = false;
            } else {
                boolean attackFlag = true;
                if (p2.getHealOp() > 0) {
                    System.out.print("Player 2 --> Heal(1) or Attack(2)?: ");
                    if(p2.getType() == PlayerType.Computer) {
                        System.out.println("2");
                    } else {
                        if(sc.nextInt() == 1) {
                            System.out.print("Enter your card ID to Heal: ");
                            int selected = sc.nextInt();
                            while(selected < 0 || selected > p1.getRemainingCards()) {
                                System.out.println(selected + " is not valid");
                                selected = sc.nextInt();
                            }
                            p2.heal(selected);
                            printBoard(p2);
                            attackFlag = false;
                        }
                    }
                }
                if (attackFlag) {
                    System.out.println();
                    System.out.println("\nPlayer (1)");
                    printBoard(p1);
                    System.out.println("\nPlayer (2)");
                    printBoard(p2);
                    System.out.print("\nPlayer 2 --> How many cards do you want to pick? ");
                    int count;
                    Random random = new Random();
                    if(p2.getType() == PlayerType.Computer) {
                        count = random.nextInt(p2.getRemainingCards()) + 1;
                        System.out.println(count);
                    } else {
                        count = sc.nextInt();
                    }
                    System.out.print("Enter your cards for attack: ");
                    ArrayList<Card> attackers = new ArrayList<>();
                    if(p2.getType() == PlayerType.Computer) {
                        while(count > 0) {
                            int randomCardID = random.nextInt(p2.getRemainingCards());
                            if (attackers.contains(p2.getCard(randomCardID))) continue;
                            attackers.add(p2.getCard(randomCardID));
                            System.out.print(randomCardID + " ");
                            count--;
                        }
                        System.out.println();
                    } else {
                        while(count > 0) {
                            int selected = sc.nextInt();
                            while(selected < 0 || selected > p2.getRemainingCards()) {
                                System.out.println(selected + " is not valid");
                                selected = sc.nextInt();
                            }
                            attackers.add(p2.getCard(selected));
                            count--;
                        }
                    }
                    System.out.print("Enter opponent card to attack: ");
                    int opponentID;
                    if(p2.getType() == PlayerType.Computer) {
                        opponentID = random.nextInt(p1.getRemainingCards());
                        System.out.println(opponentID);
                    } else
                        opponentID = sc.nextInt();
                    if (!p2.attack(attackers, p1, opponentID)) {
                        System.out.println("***************************************************");
                        System.out.println("**** Out of Energy. Please select another card ****");
                        System.out.println("***************************************************");
                        continue;
                    }
                }
                p1Turn = true;
            }

            System.out.println();

            if (p1.lost()) {
                System.out.println("*******************************");
                System.out.println("**** Player 2 won the game ****");
                System.out.println("*******************************");
                break;
            }

            if (p2.lost()) {
                System.out.println("*******************************");
                System.out.println("**** Player 1 won the game ****");
                System.out.println("*******************************");
                break;
            }
        }
    }
}
