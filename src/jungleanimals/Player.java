package jungleanimals;

import java.util.ArrayList;
import java.util.Random;

enum PlayerType {User, Computer}

public class Player {
    private ArrayList<Card> cards;
    private PlayerType type;
    private int healOp;

    public int getHealOp() {
        return healOp;
    }

    public PlayerType getType() {
        return type;
    }

    public void setType(PlayerType type) {
        this.type = type;
    }

    public Player() {
        cards = new ArrayList<>();
        healOp = 3;
    }

    public void AddCard(Card card) {
        cards.add(card);
    }

    public boolean heal(int id) {
        if(healOp <= 0)
            return false;
        cards.get(id).heal();
        healOp--;
        return true;
    }

    public Card getCard(int id)
    {
        return cards.get(id);
    }

    public void removeCard(int id) {
        cards.remove(id);
    }

    public void removeCard(Card card) {
        cards.remove(card);
    }

    public boolean attack(ArrayList<Card> attackCards, Player enemy, int id) {
        int attackPoint = 0;
        for (int i = 0; i < attackCards.size(); i++)
            attackPoint += attackCards.get(i).getAttackPoint();

        int energyCost = attackPoint / attackCards.size();

        for (int i = 0; i < attackCards.size(); i++)
            if (!attackCards.get(i).checkEnergy(energyCost)) return false;

        for (int i = 0; i < attackCards.size(); i++)
            attackCards.get(i).energyReduction(energyCost);

        if(enemy.getCard(id).hit(attackPoint))
            enemy.removeCard(id);

        return true;
    }

    public int getRemainingCards() {
        return cards.size();
    }

    public boolean lost() {
        return cards.size() == 0;
    }
}

